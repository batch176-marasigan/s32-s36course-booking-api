const express = require('express');
const route = express.Router();
const CourseController = require('../controllers/courses')
const auth = require('../auth');

//destructure the actual function that we need to use

const { verify, verifyAdmin } = auth;

//Route for Creating a course
route.post('/create', verify, verifyAdmin, (req, res) => {
	CourseController.addCourse(req.body).then(result => res.send(result))
})


//Retrieve all courses  -required token from admin
//route.get('/all', verify, verifyAdmin, (req, res) => { this for local host deve
route.get('/all',  (req, res) => { //this is fserves as backend for react get all users without verification token
	CourseController.getAllCourses().then(result => res.send(result));
})

//Retrieve all ACTIVE courses
route.get('/active', (req, res) => {
	CourseController.getAllActive().then(result => res.send(result));
})


//Retrieving a SPECIFIC course
//req.params (is short for a parameter)
// " /:parameterName"
route.get('/:courseId', (req, res) => {
	console.log(req.params.courseId)
	//we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
	CourseController.getCourse(req.params.courseId).then(result => res.send(result));
})


//Route for UPDATING a course
route.put('/:courseId', verify, verifyAdmin, (req, res) => {
	CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
})




//Archiving a course
route.put('/:courseId/archive', verify, verifyAdmin, (req, res) => {
	CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
})




module.exports = route;