//[SECTION] Dependencies and Modules
	const expresss = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const courseRoutes = require('./routes/courses');


//[SECTION] Environment Set up
	dotenv.config(); //to setup the dotenv environment.
	let account = process.env.CREDENTIALS;	
	const port = process.env.PORT;

//[SECTION] Server Setup
	const app = expresss();
	app.use(expresss.json()); 
	app.use(cors());  //enables all origins/address/url of the client request

//[SECTION] Database Connection
	mongoose.connect(account)
	 const connectStatus= mongoose.connection;
	 connectStatus.once('open',()=> console.log(`Database Connected`))

//[SECTION] Backend Routes 
	app.use('/users',userRoutes); 
	//http://localhost:4000/courses
	app.use('/courses', courseRoutes); 


//[SECTION] Server Gateway Response
	app.get('/',(req,res)=>{
		res.send('Welcome to Enrollment System')
	});
	app.listen(port,()=>{
		console.log(`API is Hosted in port ${port}`);
	});

